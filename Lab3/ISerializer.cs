﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    interface ISerializer<T>
    {
        MemoryStream Serialize(T obj);
        T Deserialize(Stream stream);
    }
}
