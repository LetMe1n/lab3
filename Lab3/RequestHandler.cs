﻿using System;
using System.IO;
using System.Net;

namespace Lab3
{
    class RequestHandler
    {
        public JsonSerializer<Output> outputSerializer;
        public JsonSerializer<Input> inputSerializer;

        public Input input = null;

        public RequestHandler()
        {
            outputSerializer = new JsonSerializer<Output>();
            inputSerializer = new JsonSerializer<Input>();
        }

        public void process(HttpListenerContext context)
        {
            try
            {
                handle(context);
            } catch(Exception ex)
            {
                
            }
        }

        protected void handle(HttpListenerContext context)
        {
            string path = context.Request.Url.AbsolutePath;
            path = path.Substring(1);
            using (RequestController controller = new RequestController(context, this))
            {
                switch (path)
                {
                    case "Ping":
                    case "Stop":
                        controller.Ping();
                        break;
                    case "PostInputData":
                        controller.PostInputData();
                        break;
                    case "GetAnswer":
                        controller.GetAnswer();
                        break;
                    default:
                        controller.NotFound();
                        break;
                }
            }
        }      
    }
}
