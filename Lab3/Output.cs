﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    [Serializable, DataContract(Namespace = "")]
    public class Output
    {
        [DataMember(Order = 1)]
        public decimal SumResult { get; set; }
        [DataMember(Order = 2)]
        public int MulResult { get; set; }
        [DataMember(Order = 3)]
        public decimal[] SortedInputs { get; set; }

        public Output() { }
    }
}
