﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            String port = Console.ReadLine();
            HTTPServer server = new HTTPServer(Int32.Parse(port));
            Console.WriteLine("Server is running on this port: " + server.Port.ToString());
        }
    }
}
