﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    [Serializable, DataContract(Namespace = "")]
    public class Input
    {
        [DataMember]
        public int K { get; set; }
        [DataMember]
        public decimal[] Sums { get; set; }
        [DataMember]
        public int[] Muls { get; set; }

        public Input() { }
    }
}
