﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    class JsonSerializer<T> : ISerializer<T>
    {
        protected DataContractJsonSerializer serializer;

        public JsonSerializer()
        {
            serializer = new DataContractJsonSerializer(typeof(T));
        }

        public MemoryStream Serialize(T obj)
        {
            MemoryStream stream = new MemoryStream();
            serializer.WriteObject(stream, obj);
            return stream;
        }

        public T Deserialize(Stream stream)
        {
            return (T)serializer.ReadObject(stream);
        }
    }
}
