﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace Lab3
{
    class RequestController : IDisposable
    {
        protected HttpListenerContext context;
        protected RequestHandler handler;

        public RequestController(HttpListenerContext context, RequestHandler handler)
        {
            this.context = context;
            this.handler = handler;
        }

        public void Ping()
        {
            context.Response.StatusCode = (int)HttpStatusCode.OK;
        }

        public void NotFound()
        {
            writeResponse("<HTML><BODY> Запрашиваемый ресурс не найден </BODY></HTML>");
        }

        public void PostInputData()
        {
            try
            {
                handler.input = handler.inputSerializer.Deserialize(context.Request.InputStream);
            } catch (Exception ex)
            {
                writeResponse("Ошибка");
            }
            
        }

        public void GetAnswer()
        {
            Output output = processing(handler.input);
            using (MemoryStream outStream = handler.outputSerializer.Serialize(output))
            {
                String result = new string(Encoding.UTF8.GetChars(outStream.ToArray()));
                writeResponse(result);
            };
        }

        protected void writeResponse(String str)
        {
            using (StreamWriter writer = new StreamWriter(context.Response.OutputStream))
            {
                writer.Write(str);
            }
        }

        protected Output processing(Input input)
        {
            Output output = new Output();
            output.SumResult = input.Sums.Select(x => x * input.K).Sum();
            output.MulResult = input.Muls.Aggregate((x, next) => x * next);
            var decimalMuls = input.Muls.Select(x => (decimal)x).ToArray();
            output.SortedInputs = decimalMuls.Concat(input.Sums).OrderBy(x => x).ToArray();

            for (int i = 0; i < output.SortedInputs.Count(); i++)
            {
                if (output.SortedInputs[i] - (int)output.SortedInputs[i] == 0)
                    output.SortedInputs[i] *= 1.0m;
            }
            return output;
        }

        public void Dispose()
        {
            context.Response.Close();
        }
    }
}
