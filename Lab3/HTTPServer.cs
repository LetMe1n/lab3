﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Lab3
{
    class HTTPServer
    {

        private Thread serverThread;
        private HttpListener listener;
        private int port;
        private RequestHandler requestHandler;

        public int Port
        {
            get { return port; }
            private set { }
        }

        public HTTPServer()
        {
            // Получаем свободный порт
            TcpListener l = new TcpListener(IPAddress.Loopback, 0);
            l.Start();
            int port = ((IPEndPoint)l.LocalEndpoint).Port;
            l.Stop();
            this.Initialize(port);
        }

        public HTTPServer(int port)
        {
            this.Initialize(port);
        }


        private void Initialize(int port)
        {
            this.port = port;
            this.requestHandler = new RequestHandler();
            serverThread = new Thread(this.Listen);
            serverThread.Start();
        }

        public void Stop()
        {
            serverThread.Abort();
            listener.Stop();
        }

        private void Listen()
        {
            listener = new HttpListener();
            listener.Prefixes.Add("http://127.0.0.1:" + port.ToString() + "/");
            listener.Start();
            while (true)
            {
                HttpListenerContext context = listener.GetContext();
                requestHandler.process(context);
                checkIfNeedStop(context);
            }
        }

        private void checkIfNeedStop(HttpListenerContext context)
        {
            string path = context.Request.Url.AbsolutePath;
            path = path.Substring(1);
            if (path == "Stop")
            {
                this.Stop();
            }
        }

    }
}
